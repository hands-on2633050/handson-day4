const main = async () => {
  const holidayAPIKey = "23a50fc3-10fd-45e0-b64f-b8dcd4dffd5e";

  let lastYearHolidays = JSON.parse(localStorage.getItem("last-year-holidays"));
  if (!lastYearHolidays) {
    try {
      const apiRes = await fetch(
        `https://holidayapi.com/v1/holidays?pretty&key=${holidayAPIKey}&country=ID&year=2022`,
        {
          //   mode: "no-cors",
          method: "GET",
          headers: {
            Accept: "application/json",
          },
        }
      );
      lastYearHolidays = await apiRes.json();
      localStorage.setItem(
        "last-year-holidays",
        JSON.stringify(lastYearHolidays)
      );
    } catch (error) {
      console.log("Something went wrong with the api.");
    }
  }

  function getDaysToEndOfMonth() {
    const today = new Date();
    const currentMonth = today.getMonth();
    const currentYear = today.getFullYear();

    const lastDay = new Date(currentYear, currentMonth + 1, 0).getDate();
    const days = [];

    for (let day = today.getDate(); day <= lastDay; day++) {
      const currentDate = new Date(currentYear, currentMonth, day);
      days.push(currentDate);
    }

    return days;
  }

  const daysToEndOfMonth = getDaysToEndOfMonth();

  daysToEndOfMonth.forEach((day) => console.log(day));

  let daysWithHolidays;

  try {
    daysWithHolidays = daysToEndOfMonth.map((date) => {
      return {
        date,
        holidays: lastYearHolidays.holidays.filter((holiday) => {
          const holidayDate = new Date(holiday.date);

          return (
            holidayDate.getDate() === date.getDate() &&
            holidayDate.getMonth() === date.getMonth()
          );
        }),
      };
    });
  } catch (error) {
    console.log("Error fetching holidays", error);
  }

  // elements
  const calendar = document.querySelector(".calendar");
  const monthLabel = document.getElementById("month-label");
  monthLabel.innerText = new Date().toLocaleDateString("en-US", {
    month: "long",
  });

  const modal = document.querySelector(".modal");
  const modalTitle = modal.querySelector(".modal__title");
  const modalBackdrop = modal.querySelector(".modal__backdrop");
  const modalCloseButton = modal.querySelector("button");
  const holidaysContainer = modal.querySelector(".holidays");

  modalBackdrop.addEventListener("click", () => {
    modal.style.display = "none";
  });

  modalCloseButton.addEventListener("click", () => {
    modal.style.display = "none";
  });

  if (calendar) {
    daysWithHolidays.forEach((day) => {
      const calendarItem = document.createElement("div");
      calendarItem.classList.add("calendar__item");

      const calendarDate = document.createElement("div");
      calendarDate.classList.add("calendar__date");
      calendarDate.innerText = day.date.getDate();

      const calendarDay = document.createElement("div");
      calendarDay.classList.add("calendar__day");
      calendarDay.innerText = day.date.toLocaleDateString("en-US", {
        weekday: "long",
      });

      const holidayBadge = document.createElement("div");
      holidayBadge.innerText = `${day.holidays?.length} holidays`;
      holidayBadge.classList.add("calendar__holidays-badge");

      calendarItem.addEventListener("click", () => {
        modal.style.display = "block";
        modalTitle.innerText = `${day.date.getDate()} ${day.date.toLocaleDateString(
          "en-US",
          { month: "long" }
        )}`;

        if (day.holidays?.length) {
          holidaysContainer.innerHTML = "";
          day.holidays.map((holiday) => {
            const holidayEl = document.createElement("div");
            holidayEl.classList.add("holiday");
            holidayEl.innerText = holiday.name;

            holidaysContainer.appendChild(holidayEl);
          });
        } else {
          holidaysContainer.innerHTML = "No holidays this day.";
        }
      });

      calendarItem.appendChild(calendarDate);
      calendarItem.appendChild(calendarDay);
      calendarItem.appendChild(holidayBadge);

      calendar.appendChild(calendarItem);
    });
  }
};

main();
